# Staging Systems

Minimales Projekt um zu zeigen mit wie wenig Aufwand ein Staging System pro Branch aufzusetzen ist sobald die Infrastruktur dafür steht. 
Server die dafür nötig sind: 
* GitLab
* GitLab Runner mit docker als executer eingestellt 
* Rancher 
* einen Server auf dem die Stagings hochgefahren werden.
 
Dazu werden 3 Files gebraucht um dann dieses minimal Beispiel zum laufen zu bekommen. 
Zu einem die `gitlab-ci.yml` für das CI und dann noch ein `Dockerimage` sowie eine `docker-compose.yml`.
Mehr ist dafür dann nicht nötig.

Klicke [hier](https://master.staging-systems.daniel-badura.de/) um die Review Umgebung vom Master zu sehen.

